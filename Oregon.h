/*
    Name:       TestCodeATTiny84.ino
    Created:	29/05/2019 22:46:47
    Author:     DESKTOP-VSODJL3\NicoD

    Oregon Functions copied from into a cpp library.
    Cr�dits to Olivier Lebrun
*/
/*
 * connectingStuff, Oregon Scientific v2.1 Emitter
 * http://connectingstuff.net/blog/encodage-protocoles-oregon-scientific-sur-arduino/
 *
 * Copyright (C) 2013 olivier.lebrun@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
// Oregon.h

#ifndef _OREGON_h
#define _OREGON_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include "Config.h"

#define SENDHIGH() digitalWrite(TX_PIN, HIGH)
#define SENDLOW() digitalWrite(TX_PIN, LOW)
const unsigned long TIME = 512;
const unsigned long TWOTIME = TIME * 2;


inline void sendZero(void);
inline void sendOne(void);
inline void sendQuarterMSB(const byte data);
inline void sendQuarterLSB(const byte data);
void sendData(byte* data, byte size);;
void sendOregon(byte* data, byte size, char type);
inline void sendPreamble(void);
inline void sendPostamble(char type);
inline void sendSync(void);

void setType(byte* data, byte* type);
void setChannel(byte* data, byte channel);
void setId(byte* data, byte ID);
void setBatteryLevel(byte* data, byte level);
void setTemperature(byte* data, float temp);
void setHumidity(byte* data, byte hum);
int Sum(byte count, const byte* data);
void calculateAndSetChecksum(byte* data, char type);






#endif

