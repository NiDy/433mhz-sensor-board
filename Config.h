/*
    Name:       TestCodeATTiny84.ino
    Created:	29/05/2019 22:46:47
    Author:     DESKTOP-VSODJL3\NicoD
*/
/*
Configuration file. Modify this to change the program behaviour.
Import this file in every library needed. 
*/
#pragma once

/************************************************************
////////////////////////////////////////////////////////////
****************      Configuration     *******************/

///////////////////////////////////
//Define the LOW voltage 
#define LOW_BATTERY_LEVEL_LIION 3500L // Voltage minumum (mV) 

///////////////////////////////////
//Define the delay between send
#define WDT_COUNT 10L        //Send ever 10L x 8seconds. For example "8L" => send every 64seconds. 37L => 5min.

///////////////////////////////////
//Define the Node ID 
#define NODE_ID 10     //0x0A in hexa 

///////////////////////////////////
//Define the sensor to adapt the code different behaviour. Uncomment ONLY ONE
//#define CAPACITIVE	
#define DS18B20 //Wiring note : do not add the 4,7kR resistor as it is included in this modified version of OneWire

///////////////////////////////////
//Define a send method. Comment to remove
//#define SEND_FINEOFFSET		//fineoffset	
#define SEND_OREGON			//Oregon (heavy)

///////////////////////////////////


/***************  end  configuration   *****************/
////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////
//Pin Map
#define PWR_SENSOR 1
#define DATA_PIN_1 2   
#define DATA_PIN_2 3   
#define DATA_PIN_3 0 
#define MOSI 6
#define MISO 5
#define RST_EEP MOSI
#define TX_PIN 9                  
#define LED 4

#define WATCHDOG_MODE 9 //9=8s. See setup_watchdog() for the desc of the modes. 

#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

//Oregon CHannel possibilities
const int chan[] = { 0x10,0x20,0x30 };

#ifndef SENDHIGH()
#define SENDHIGH() digitalWrite(TX_PIN, HIGH)
#define SENDLOW() digitalWrite(TX_PIN, LOW)
#endif // !SENDHIGH()


//SENSOR SPECIFIC INCLUDES
#ifdef DS18B20
#include "OneWire.h"
#include "DallasTemperature.h"
#endif


////////////////////////////////////////////////
/////////// TX Functions
#ifdef SEND_OREGON
#include "Oregon.h"
#endif

#ifdef SEND_FINEOFFSET
#include "FineOffset.h"
#endif
