/*
    Name:       TestCodeATTiny84.ino
    Created:	29/05/2019 22:46:47
    Author:     NicolasDAUY
*/
/*
	Example start code for the "Cheap 433Mhz sensor kit" product sold on Tindie
	https://www.tindie.com/products/16134/

	Description of the files : 
		- .ino : arduino file with setup() and infinite loop()
		- Config.h : this file should be included in all other files. It contains the global definitions of the Tx protocol, the sensors, etc...
		- Global.h & .cpp : this contains global functions, including the GetSensors(). The gaol is only to keep the .ino clean 
		- TX protocols library : Oregon.cpp, FineOffset.cpp
		- Sensors library : 
			* DS18B20 : DallasTemperature.h, OneWire.h
			* Capacitive soil moisture sensor : No file as it consists of an AnalogRead()

	Description of the process
		- The watchdog timer is enabled at 8 seconds. It throws an Interruption, and then enbales the chip to wake up 
		- The setup initiliazes the sensors, and the Tx functions
		- In the loop, it goes immediatly to sleep cpu
		- Every 8 seconds, it is waken up by the watchdog, it blinks, check if the sending delay is passed
			- if passed, it measure the sensor, and sends it over 433MHz RF
			- if not it goes back to sleep immediatly

*/

// Common librairies
#include "Config.h" //Use this file to change the behaviour 
#include "Global.h" //


////////////////////////////////////////////////
/////////// FINEOFFSET Initialisation
#ifdef SEND_FINEOFFSET
FineOffset my_fineoffset(TX_PIN);
#endif // SEND_FINEOFFSET

////////////////////////////////////////////////
/////////// OREGON Initialisation
#ifdef SEND_OREGON
//Buffer (8 for temperature only, 9 for temp + hum
#ifdef CAPACITIVE
byte OregonMessageBuffer[9];
#endif // CAPACITIVE
#if defined(DS18B20) && !defined(CAPACITIVE)//If only temp, better choose this kind of message
byte OregonMessageBuffer[8];
#endif // CAPACITIVE
#endif



////////////////////////////////////////////////
/////		GENERAL Functions 
//Ini of some variables for the process 
volatile unsigned int count = 0; //count for the watchdog
int mode_wdt = WATCHDOG_MODE;	//can be changed dynamically
boolean lowBattery = false;		//
long Low_Batt_Level = LOW_BATTERY_LEVEL_LIION;//valeur d�faut

// Watchdog Interrupt Service / is executed when watchdog timed out 
ISR(WDT_vect) {
	//wake up
	count--;
}



////////////////////////////////////////////////
void setup()
{
	delay(1000);
	//CLKPR = (1<<CLKPCE);  
	//CLKPR = B00000000;  // set the fuses to 8mhz clock-speed.

	setup_watchdog(mode_wdt, 1);
	pinMode(TX_PIN, OUTPUT); // sortie transmetteur
	pinMode(MOSI, INPUT); // 
	pinMode(MISO, INPUT); //
	pinMode(TX_PIN, OUTPUT); // sortie transmetteur
	pinMode(PWR_SENSOR, OUTPUT); // sortie transmetteur
	pinMode(LED, OUTPUT); digitalWrite(LED, HIGH);
	delay(4000);

	// Ini OREGON
#ifdef SEND_OREGON
#ifdef CAPACITIVE //message format case Humidity
	byte ID[] = { 0x1A,0x2D };
#endif // CAPACITIVE
#ifdef PRESSURE_ATM
	byte ID[] = { 0x5A, 0x6D };
#endif // PRESSURE_ATM
#if defined(DS18B20) && !defined(CAPACITIVE) //message format case Temp Only
	byte ID[] = { 0xEA,0x4C };
#endif // DS18B20

	//Ini oregon 
	setType(OregonMessageBuffer, ID);
	setChannel(OregonMessageBuffer, chan[1]);	//Choose channel 
	setId(OregonMessageBuffer, NODE_ID);		//Choose Node ID
#endif // SEND_OREGON

	//Blink if there is a problem with Sensor initilisation
	uint8_t state;
	while (!iniSensors()) {
		digitalWrite(LED, state++%2); delay(100); 
	}

	digitalWrite(PWR_SENSOR, LOW);
	digitalWrite(LED, LOW);

	//envoi batterie d�marrage
	//send_battery();//X10RF Only 
	//for oregon
	SENDLOW();
}

void loop()
{
	//blink low power
	digitalWrite(LED, HIGH); delay(10); digitalWrite(LED, LOW); 

	if (count <= 0) { // we wait watchdog end sleep cycles number

		pinMode(PWR_SENSOR, OUTPUT); digitalWrite(PWR_SENSOR, HIGH);
		//wait for power to stabilize
		delay(50);
		count = WDT_COUNT;  // reset counter

		// Get Temperature, humidity and battery level from sensors
		float temp, hum;
		if (getSensor(&temp, &hum)) {
			// we need round temp to one decimal...
			int a = round(temp * 10);
			temp = a / 10.0;
			//hum in byte
			uint8_t hum_byte = (uint8_t)round(hum);
			// Get the battery state
			lowBattery = readVCC() < Low_Batt_Level;
#ifdef SEND_OREGON
				// Set Battery Level
				setBatteryLevel(OregonMessageBuffer, !lowBattery);  // 0=low, 1=high
				// Set Temperature
				setTemperature(OregonMessageBuffer, temp);
				char type = 't';//type t = temp only, h = temp +hum, p=temp+hum+pressure
#ifdef CAPACITIVE
				// Set Humidity
				setHumidity(OregonMessageBuffer, hum_byte);
				type = 'h';
#endif // CAPACITIVE

				// Calculate the checksum
				calculateAndSetChecksum(OregonMessageBuffer, type);
				// Send the Message over RF
				sendOregon(OregonMessageBuffer, sizeof(OregonMessageBuffer), type);
				// Send a "pause"
				SENDLOW();
				delayMicroseconds(TWOTIME * 8);
				// Send a copie of the first message. The v2.1 protocol send the message two time 
				sendOregon(OregonMessageBuffer, sizeof(OregonMessageBuffer), type);
#endif // SEND_OREGON
#if defined(SEND_FINEOFFSET) && defined(SEND_OREGON)
				delay(200);
#endif
#ifdef SEND_FINEOFFSET
				my_fineoffset.send(NODE_ID, temp, hum_byte);
#endif // SEND_FINEOFFSET
				//put the Tx module in sleep
				SENDLOW();
		}

		//Patch for DHT11 and DHT22 : data pin left OUTPUT HIGH by the library
		digitalWrite(PWR_SENSOR, LOW); pinMode(PWR_SENSOR, INPUT);

	}
	
	system_sleep();
}
