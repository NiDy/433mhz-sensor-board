/*
	Name:       TestCodeATTiny84.ino
	Created:	29/05/2019 22:46:47
	Author:     DESKTOP-VSODJL3\NicoD
*/
/*
Set of global function used in the .ino

*/

#include "Global.h"
#include "Config.h"
#include <avr/sleep.h>    // Sleep Modes
#include <avr/wdt.h>      // Watchdog timer
#include <avr/interrupt.h>



//SENSORS GLOBAL DECLARATION
#ifdef DS18B20
OneWire ds(DATA_PIN_3); // Cr�ation de l'objet OneWire ds
DallasTemperature sensors(&ds);
#endif 


// set system into the sleep state 
// system wakes up when wtchdog is timed out
void system_sleep() {
	cbi(ADCSRA, ADEN);                    // switch Analog to Digitalconverter OFF
	set_sleep_mode(SLEEP_MODE_PWR_DOWN); // sleep mode is set here
	sleep_mode();                        // Go to sleep
	sbi(ADCSRA, ADEN);                    // switch Analog to Digitalconverter ON
}


// 0=16ms, 1=32ms,2=64ms,3=128ms,4=250ms,5=500ms
// 6=1 sec,7=2 sec, 8=4 sec, 9= 8sec
void setup_watchdog(int ii, boolean interrupt) {
	byte bb;
	int ww;
	if (ii > 9) ii = 9;
	bb = ii & 7;
	if (ii > 7) bb |= (1 << 5);
	bb |= (1 << WDCE);
	ww = bb;

	MCUSR &= ~(1 << WDRF);
	// start timed sequence
#if defined(__AVR_ATtiny84__)
#define WDTCR WDTCSR
#endif
	WDTCR |= (1 << WDCE) | (1 << WDE);
	// set new watchdog timeout value
	WDTCR = bb;
	if (interrupt) WDTCR |= _BV(WDIE);
	else WDTCR &= ~(_BV(WDIE));
}
//



//reads internal 1V1 reference against VCC
//return number 0 .. 1023 
int analogReadInternal(void) {
	ADMUX = _BV(MUX5) | _BV(MUX0); // For ATtiny84
	delay(5); // Wait for Vref to settle
	ADCSRA |= _BV(ADSC); // Convert
	while (bit_is_set(ADCSRA, ADSC));
	uint8_t low = ADCL;
	return (ADCH << 8) | low;
}

//calculate VCC based on internal referrence
//return voltage in mV
int readVCC() {
	//Calibration 
	float factor_A = 1;	//y=Ax+B
	int factor_B = 0;	//y=Ax+B

	return (int)(((((uint32_t)1023 * (uint32_t)1100) / analogReadInternal()) * factor_A) + factor_B);
}

void send_battery() {//X10RF Only
	digitalWrite(LED, LOW);
	sbi(ADCSRA, ADEN);  // switch Analog to Digitalconverter ON
	delay(100);
	// Send battery readings 
	readVCC();
	int vcc = readVCC();
	//myx10.RFXsensor(NODE_ID, 'v', 0, (uint8_t)(round(vcc / 100)));
}



///////////////////////////////////////////
//////		SENSORS			//////////////
//////////////////////////////////////////
//INI SENSORS
boolean iniSensors(void) {
		//INI Sensors
	#ifdef CAPACITIVE
		pinMode(DATA_PIN_3, INPUT); //Analogique
	#endif // CAPACITIVE
	#if defined(DS18B20) 
		delay(20);
		//besoin d'alimenter pour trouver 
		sensors.begin();
		sensors.setResolution(11);
	#endif
	return true;
}

// GET SENSOR 
boolean getSensor(float* temp1, float* hum) {
	*temp1 = *hum = 0;
	boolean answer=1;
#ifdef CAPACITIVE
	delay(200);
	uint8_t nb_measures = 5;
	//for some reason, the first value might be higher than all the others due to sensors wake up.
	analogRead(DATA_PIN_1);
	float ana = 0.0;
	delay(10);
	// Pressure sensor seems subject to noise. With take a 20 values
	for (uint8_t i = 0; i < nb_measures; i++) {
		ana += analogRead(DATA_PIN_1);
		delay(10);
	}
	float mesure = round(ana / nb_measures);

	//CALIBRATION values 
	mesure > 650 ? mesure = 649 : mesure = mesure;
	mesure < 300 ? mesure = 301 : mesure = mesure;
	*hum = map(mesure, 650, 300, 0, 100);

	answer &= 1;//put here &= if a strict verification is needed

#endif

#ifdef DS18B20
	sensors.requestTemperatures();
	delay(375);//375 EN r�solution 11
	*temp1 = sensors.getTempCByIndex(0);
	//*temp1=28;
	digitalWrite(DATA_PIN_3, LOW); pinMode(DATA_PIN_3, INPUT);
	answer &= 1; //put here &= if a strict verification is needed
#endif
	return answer;
}

