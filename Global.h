/*
    Name:       TestCodeATTiny84.ino
    Created:	29/05/2019 22:46:47
    Author:     DESKTOP-VSODJL3\NicoD
*/
/*
Set of global function used in the .ino

*/
// Global.h

#ifndef _GLOBAL_h
#define _GLOBAL_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

void system_sleep(void); 
void setup_watchdog(int ii, boolean interrupt);

int analogReadInternal(void);
int readVCC(void);
void send_battery(void);

boolean iniSensors(void);
boolean getSensor(float* temp, float* hum);

#endif

